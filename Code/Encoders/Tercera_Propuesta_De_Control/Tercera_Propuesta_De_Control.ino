#define encD  2       // Encoder derecho
#define encI  3       // Encoder Izquierdo

#define E1  8         // PWM2 Speed Control Derecho              analogWrite (E1,#);
#define E2  9         // PWM1 Speed Control Izquierdo
#define M1  10        // EN2 Direction Control Derecho        digitalWrite(M1,HIGH/LOW);
#define M2  11        // EN1 Direction Control Izquierdo

byte PWM = 80;       // Velocidad del carrito
byte PWM_Max = PWM + 30;     // Velocidad máxima del carrito
byte PWM_Min = PWM - 50;      // Velocidad mínima del carrito

byte Velocidad_D = PWM;       // Velocidad inicial del motor derecho
byte Velocidad_I = PWM;       // Velocidad inicial del motor izquierdo

unsigned long Tick_D = 0;
unsigned long Tick_I = 0;

// OTRAS VARIABLES
bool estado = LOW;
#define Push 22

void setup() {
  // Variables para promedio
  Serial.begin(115200);       // Inicialización de la comunicación serial

  // Variables para carrito
  pinMode(E1, OUTPUT);       // PIN de velocidad para el motor derecho como salida
  pinMode(E2, OUTPUT);       // PIN de velocidad para el motor izquierdo como salida
  pinMode(M1, OUTPUT);        // PIN de dirección del motor derecho como salida
  pinMode(M2, OUTPUT);        // PIN de dirección del motor izquierdo como salida

  // Otras variables
  pinMode(Push, INPUT);

  // Linea para añadir una interrupciòn a un PIN
  attachInterrupt(digitalPinToInterrupt(encD), EncoderD, RISING);
  attachInterrupt(digitalPinToInterrupt(encI), EncoderI, RISING);

}

void loop() {
  avanzar(Velocidad_D, Velocidad_I);

  if (Tick_D > Tick_I+1) {
    Velocidad_D = Velocidad_D - 1;
  } else if (Tick_I  > Tick_D+1) {
    Velocidad_I = Velocidad_I - 1;
  } else {
    Velocidad_D = PWM;
    Velocidad_I = PWM;
  }

  if (Velocidad_D < PWM_Min) {
    Velocidad_D = PWM_Min;
  }
  if (Velocidad_I < PWM_Min) {
    Velocidad_I = PWM_Min;
  }

  Serial.print("Vel_D: ");
  Serial.print(Velocidad_D);
  Serial.print("   Tick_D: ");
  Serial.print(Tick_D);
  Serial.print("   Vel_I: ");
  Serial.print(Velocidad_I);
  Serial.print("   Tick_I: ");
  Serial.println(Tick_I);



}


void avanzar(char a, char b) {        // Moverse hacia adelante
  analogWrite (E1, b);
  //  digitalWrite(M1, HIGH);
  analogWrite (E2, a);
  //  digitalWrite(M2, HIGH);
  //  Serial.print(Tick_D);
  //  Serial.print(",");
  //  Serial.println(Tick_I);

}

void EncoderD () {
  Tick_D = Tick_D + 1;
  //      Serial.print("Tick Derecho: ");
  //      Serial.println(Tick_D);
}

void EncoderI () {
  Tick_I = Tick_I + 1;
  //      Serial.print("                 Tick Izquierdo: ");
  //      Serial.println(Tick_I);
}
