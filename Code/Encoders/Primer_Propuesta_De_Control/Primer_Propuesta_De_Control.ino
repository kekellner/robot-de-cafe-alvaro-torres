#include <math.h>                                        // necesaria para utilizar función atan()
#define PI 3.1415926535897932384626433832795             // definición del número PI

///----------------------------------- Variables Principales -------------------------------------------------
float Xd = 120;                                          // POSICIÓN X DESEADA
float Yd = 0;                                            // POSICIÓN Y DESEADA
float V = 60;                                            // VELOCIDAD DEL CARRO
int PWMmax = 90;                                         // PWM máximo
int PWMmin = 30;                                         // PWM mínimo

int prueba = 0;
///------------------------------- Variables Posición del robot ---------------------------------------------
float Cdistancia = 0;                                    // distancia recorrido por el punto central
float x = 0;                                             // distancia recorrida eje X
float y = 0;                                             // distancia recorrida eje Y
float phi = 0;                                           // posición angular


///------------------------------- Variables Posición deseada ----------------------------------------------
float Phid = atan2(Yd - y, Xd - x);                      // ÁNGULO


///------------------------------- Variables y valores iniciales --------------------------------------------
int caso = 0;                                            // caso para seleccionar trayectoria
int N = 20;                                              // número de ranuras del encoder
int contadorTicks = 1;                                   // número de ticks para cálculo de velocidad (recordar que entre menor sea el valor mayor ruido de la médida)
int tam = 10;                                            // tamaño del vector del cálculo de promedio (Este valor depende del tamaño de los vectores de promedio vectorL y vectorR)
int k = 10;                                              // tiempo de muestreo
int alto = 0;                                            // variable para detener ambos motores indefinidamente
int limite = 1;                                          // error aceptable entre posición actual y posición deseada
//int limite = 4;                                        // error aceptable entre posición actual y posición deseada

volatile unsigned muestreoActual = 0;                    // variables para definición del tiempo de muestreo
volatile unsigned muestreoAnterior = 0;
volatile unsigned deltaMuestreo = 0;

float error = 0;                                         // error variables
float Kp = 40;                                           // contante proporcional control
int PWMr = 0;                                            // PWM de la llanta derecha (señal de control llanta derecha)
int PWMl = 0;                                            // PWM de la llanta izquierda (señal de control llanta izquierda)


///------------------------------- Variables del robot  ----------------------------------------------------
float diametro = 6.8;                                    // diámetro de la llanta cm
float longitud = 13.4;                                   // longitud del robot entre llantas
float W = 0;                                             // velocidad angular del carro


///------------------------------- Variables de motor derecho-----------------------------------------------
volatile unsigned muestreoActualInterrupcionR = 0;       // variables para definición del tiempo de interrupción y cálculo de la velocidad motor derecho
volatile unsigned muestreoAnteriorInterrupcionR = 0;
double deltaMuestreoInterrupcionR = 0;

int encoderR = 3;                                        // pin de conexión del encoder derecho
int llantaR = 8;                                         // pin de conexión de llanta derecha   (pin de PWM)
int M1 = 10;     // PWM1 Direction Control Derecho        digitalWrite(M1,HIGH/LOW);

double frecuenciaR = 0;                                  // frecuencia de interrupción llanta R
double Wr = 0;                                           // velocidad angular R
double Vr = 0;                                           // velocidad lineal
int CR = 0;                                              // contador ticks
float vectorR[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};        // vector de almacenamiento de datos para promedio del tiempo de interrupciones

float Rdistancia = 0;                                    // distancia recorrida llanta derecha
int Rtick = 0;                                           // ticks del encoder derecho
int RtickAnt = 0;                                        // ticks del encoder derecho anteriores
int deltaRtick = 0;                                      // diferencia del encoder derecho


///------------------------------  Variables de motor izquierdo ------------------------------------------------
volatile unsigned muestreoActualInterrupcionL = 0;       // variables para definición del tiempo de interrupción y cálculo de la velocidad motor Izquierdo
volatile unsigned muestreoAnteriorInterrupcionL = 0;
double deltaMuestreoInterrupcionL = 0;

int encoderL = 2;                                        // pin de conexión del encoder Izquierdo
int llantaL = 9;                                         // pin de conexión de llanta Izquierda   (pin de PWM)
int M2 = 11;     // PWM2 Direction Control Izquierdo

double frecuenciaL = 0;                                  // frecuencia de interrupción llanta Izquierda
double Wl = 0;                                           // velocidad angular L
double Vl = 0;                                           // velocidad Lineal
int CL = 0;                                              // contador Ticks
float vectorL[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};        // vector de almacenamiento de datos para promedio del tiempo de interrupciones

float Ldistancia = 0;                                    // distancia recorrida llanta izquierda
int Ltick = 0;                                           // ticks del encoder izquierdo
int LtickAnt = 0;                                        // ticks del encoder izquier anteriores
int deltaLtick = 0;                                      // diferencia del encoder izquierdo


///------------------------------  Set up ----------------------------------------------------------------------
void setup() {
  attachInterrupt(digitalPinToInterrupt(encoderR), REncoder, FALLING);              // linea para añadir una interrupción a un PIN
  attachInterrupt(digitalPinToInterrupt(encoderL), LEncoder, FALLING);              // linea para añadir una interrupción a un PIN
  Serial.begin(9600);                                                               // inicio de la comunicación serial
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
}


///------------------------------  Interrupciones --------------------------------------------------------------
void REncoder() {                                        // función de interrupción del enconder llanta derecha
  Rtick++;                                               // número de ticks llanta derecha
  CR++;                                                  // incremento del contador de ticks
  if (CR == contadorTicks) {                             // si el contador de ticks alcanza el valor de ticks determinado para el cálculo del tiempo
    float media = 0;                                     // variable creada para cálculo del promedio

    // Filtro promedio
    //-----------------------------------------------------
    for (int i = 0; i < tam - 1; i++) {                  // relleno del vector para cálculo posterior del promedio
      vectorR[i] = vectorR[i + 1];
    }
    vectorR[tam - 1] = deltaMuestreoInterrupcionR;       // último dato del vector (medida actual)

    for (int i = 0; i < tam; i++) {                      // suma de los valores del vector
      media = vectorR[i] + media;
    }
    media = media / tam;                                 // división por el total de datos del vector
    deltaMuestreoInterrupcionR = media;                  // se reemplaza por el valor de su medio.

    //-----------------------------------------------------
    frecuenciaR = (1000) / deltaMuestreoInterrupcionR;                    // frecuencia de interrupción
    muestreoAnteriorInterrupcionR = muestreoActualInterrupcionR;          // se actualiza el tiempo de interrupción anterior
    CR = 0;                                                               // reinicio de contador de ticks
  }
}


void LEncoder() {                                        // función de interrupción del enconder llanta izquierda
  Ltick++;                                               // número de ticks llanta izquierda
  CL++;                                                  // incremento del contador de ticks
  if (CL == contadorTicks) {                             // si el contador de ticks alcanza el valor de ticks determinado para el cálculo del tiempo
    float media = 0;                                     // variable creada para cálculo del promedio

    // Filtro promedio
    //-----------------------------------------------------
    for (int i = 0; i < tam - 1; i++) {                  // relleno del vector para calculo posterior del promedio
      vectorL[i] = vectorL[i + 1];
    }
    vectorL[tam - 1] = deltaMuestreoInterrupcionL;       // último dato del vector (medida actual)

    for (int i = 0; i < tam; i++) {                      // suma de los valores del vector
      media = vectorL[i] + media;
    }
    media = media / tam;                                 // división por el total de datos del vector
    deltaMuestreoInterrupcionL = media;                  // se reemplaza por el valor de su medio.

    //-----------------------------------------------------
    frecuenciaL = (1000) / deltaMuestreoInterrupcionL;                 // frecuencia de interrupción
    muestreoAnteriorInterrupcionL = muestreoActualInterrupcionL;       // se actualiza el tiempo de interrupción anterior
    CL = 0;                                                            // reinicio de contador de ticks
  }
}


///------------------------------  LOOP  -----------------------------------------------------------------------
void loop() {
  muestreoActual = millis();                             // tiempo actual de muestreo
  muestreoActualInterrupcionR = millis();                // se asigna el tiempo de ejecución a el muestreo actual
  muestreoActualInterrupcionL = millis();                // se asigna el tiempo de ejecución a el muestreo actual

  deltaMuestreo = (double) muestreoActual - muestreoAnterior;                // delta de muestreo

  if ( deltaMuestreo >= k)                               // se asegura el tiempo de muestreo
  {
    rutas();
    Phid = atan2(Yd - y, Xd - x);                        // recalcular el ángulo deseado en cada iteración, dado que el cambia con respecto a cada movimiento

    deltaMuestreoInterrupcionR = muestreoActualInterrupcionR -  muestreoAnteriorInterrupcionR;       // diferencia tiempos de interruciones de ticks del motor
    deltaMuestreoInterrupcionL = muestreoActualInterrupcionL -  muestreoAnteriorInterrupcionL;       // diferencia tiempos de interruciones de ticks del motor

    if (deltaMuestreoInterrupcionR >= 200 * contadorTicks) {                // esta es la forma de definir cuando el motor se encuentra quieto. Si deltaMuestreoInterrupcionR es mayor a 40 milisegundos por el preescalado de ticks
      frecuenciaR = 0;
    }                                                     // 40 mS es el tiempo que màximo se tarda un tick a la menor velocidad del motor


    if (deltaMuestreoInterrupcionL >= 200 * contadorTicks) {                // Esta es la forma de definir cuando el motor se encuentra quieto. Si deltaMuestreoInterrupcionR es mayor a 40 milisegundos por el preescalado de ticks
      frecuenciaL = 0;
    }                                                     // 40 mS es el tiempo que màximo se tarda un tick a la menor velocidad del motor


    Wr = contadorTicks * ((2 * PI) / N) * frecuenciaR;                      // frecuencia angular Rad/s
    Vr = Wr * (diametro / 2);                                               // velocidad lineal cm/s
    Wl = contadorTicks * ((2 * PI) / N) * frecuenciaL;                      // frecuencia angular Rad/s
    Vl = Wl * (diametro / 2);                                               // velocidad lineal cm/s

    // velocidad constante para alcanzar el ángulo (controlador)
    //-----------------------------------------------------
    error = Phid - phi;                                  // error angular ángulo deseado menos el ángulo del robot
    W = (Vr - Vl) / longitud + Kp * error;               // cálculo de la velocidad angular con las variables de control
    PWMr = V + (W * longitud) / 2;                       // señal de control PWM llanta derecha
    PWMl = V - (W * longitud) / 2;                       // señal de control PWM llanta izquierda


    // Límite inferior y superior para la velocidad
    //-----------------------------------------------------
    if (PWMr > PWMmax) {                                 // si el motor derecho sobrepasa el límite superior
      PWMr = PWMmax;
    }                                 // establezco la velocidad del motor derecho con el límite superior

    if (PWMr < PWMmin) {                                 // si el motor derecho sobrepasa el límite inferior
      PWMr = PWMmin;
    }                                 // establezco la velocidad del motor derecho con el límite inferior

    if (PWMl > PWMmax) {                                 // si el motor izquierdo sobrepasa el límite superior
      PWMl = PWMmax;
    }                                 // establezco la velocidad del motor izquierdo con el límite superior

    if (PWMl < PWMmin) {                                 // Si el motor izquierdo sobrepasa el límite inferior
      PWMl = PWMmin;
    }                                 // establezco la velocidad del motor izquierdo con el límite inferior



    // Si llegó al púnto específico se detendrá
    //-----------------------------------------------------
    if ( abs(x - Xd) < limite && abs(y - Yd) < limite) {          // si la posición X y Y llegá a una posición aceptable del punto indicado
      analogWrite(llantaR, 0);                                    // detener el boton derecho
      analogWrite(llantaL, 0);                                    // detener el boton izquierdo
      alto = 1;

            while (alto == 1) {
              delay(2000);                                              // detengo los motores indefinidamente
              caso = caso + 1;                                          // cambio de caso para realizar giro
      //        if (caso > 4) {                                           // si supero mi número de posibles rutas establezco la primera
      //          caso = 1;
      //        }
      //        alto = 0;
            }
    }


    // Avanzar si no ha llegado al punto específico
    //-----------------------------------------------------
    else {
      avanzar();
    }

    odometria();                                         // cálculo de la odometría

    Serial.print(x);                                     // se muestra el tiempo entre TIC y TIC
    Serial.print(",");                                   // se muestra el tiempo entre TIC y TIC
    Serial.println(y);                                   // se muestra el tiempo entre TIC y TIC

    muestreoAnterior = muestreoActual;                   // actualización del muestreo anterior
  }
}



///------------------------------  Funciones -------------------------------------------------------------------
void avanzar () {
  analogWrite(llantaR, PWMr);
  analogWrite(llantaL, PWMl);
}

void rutas() {
  switch (caso) {
    case 0:
      Xd = Xd;                                        // POSICIÓN X DESEADA
      Yd = Yd;
      break;

    //    case 1:
    //      digitalWrite(M1, HIGH);
    //      analogWrite(llantaR, 60);
    //      analogWrite(llantaL, 60);
    //      delay(800);
    //      analogWrite(llantaR, 0);                                    // detener el boton derecho
    //      analogWrite(llantaL, 0);                                    // detener el boton izquierdo
    //      delay(800);
    //      alto = 1;
    //      while (alto == 1) {
    //        digitalWrite(M1, LOW);
    //        Xd = 20;                                        // POSICIÓN X DESEADA
    //        Yd = 0;
    //        caso = 2;
    //        reinicio();
    //      }
    //      break;
    //
    //    case 2:
    //      Xd = 20;                                        // POSICIÓN X DESEADA
    //      Yd = 0;
    //      break;
    //
    //    case 3:
    //      digitalWrite(M1, HIGH);
    //      analogWrite(llantaR, 60);
    //      analogWrite(llantaL, 60);
    //      delay(900);
    //      analogWrite(llantaR, 0);                                    // detener el boton derecho
    //      analogWrite(llantaL, 0);                                    // detener el boton izquierdo
    //      delay(800);
    //      alto = 1;
    //      while (alto == 1) {
    //        digitalWrite(M1, LOW);
    //        Xd = 20;                                        // POSICIÓN X DESEADA
    //        Yd = 0;
    //        caso = 4;
    //        reinicio();
    //      }
    //      break;
    //
    //    case 4:
    //      digitalWrite(13, HIGH);
    //      alto = 1;
    //      while (alto == 1) {
    //        analogWrite(llantaR, 0);                                    // detener el boton derecho
    //        analogWrite(llantaL, 0);
    //      }
    //      break;

    default:
      analogWrite(llantaR, 0);                                    // detener el boton derecho
      analogWrite(llantaL, 0);                                    // detener el boton izquierdo
      break;
  }
}

void odometria() {
  deltaRtick = Rtick - RtickAnt;                                  // comparación de los ticks recorridos desde el último cálculo llanta derecha
  Rdistancia = PI * diametro * (deltaRtick / (double) 20);        // distancia recorrida por la llanta derecha desde el último cálculo

  deltaLtick = Ltick - LtickAnt;                                  // comparación de los ticks recorridos desde el último cálculo llanta izquierda
  Ldistancia = PI * diametro * (deltaLtick / (double) 20);        // distancia recorrida por la llanta izquierda desde el último cálculo

  Cdistancia = (Rdistancia + Ldistancia) / 2;                     // distancia recorrida por el punto central desde el último cálculo

  x = x + Cdistancia * cos(phi);                                  // posición del punto X actual
  y = y + Cdistancia * sin(phi);                                  // posición del punto Y actual

  phi = phi + ((Rdistancia - Ldistancia) / longitud);             // posición angular actual
  phi = atan2(sin(phi), cos(phi));                                // transformación de la posición angular entre -PI y PI

  RtickAnt = Rtick;                                               // actualización de la variable RtickAnt con los valores de Rtick
  LtickAnt = Ltick;                                               // actualización de la variable LtickAnt con los valores de Ltick
}




void reinicio() {
  V = 60;                                            // VELOCIDAD DEL CARRO                                         // PWM mínimo
  Cdistancia = 0;                                    // distancia recorrido por el punto central
  x = 0;                                             // distancia recorrida eje X
  y = 0;                                             // distancia recorrida eje Y
  phi = 0;                                           // posición angular
  Phid = atan2(Yd - y, Xd - x);                      // ÁNGULO
  contadorTicks = 1;                                   // número de ticks para cálculo de velocidad (recordar que entre menor sea el valor mayor ruido de la médida)
  tam = 10;                                            // tamaño del vector del cálculo de promedio (Este valor depende del tamaño de los vectores de promedio vectorL y vectorR)
  k = 10;                                              // tiempo de muestreo
  alto = 0;                                            // variable para detener ambos motores indefinidamente
  muestreoActual = 0;                    // variables para definición del tiempo de muestreo
  muestreoAnterior = 0;
  deltaMuestreo = 0;
  error = 0;                                         // error variables
  PWMr = 0;                                            // PWM de la llanta derecha (señal de control llanta derecha)
  PWMl = 0;                                            // PWM de la llanta izquierda (señal de control llanta izquierda)
  muestreoActualInterrupcionR = 0;       // variables para definición del tiempo de interrupción y cálculo de la velocidad motor derecho
  muestreoAnteriorInterrupcionR = 0;
  deltaMuestreoInterrupcionR = 0;
  frecuenciaR = 0;                                  // frecuencia de interrupción llanta R
  Wr = 0;                                           // velocidad angular R
  Vr = 0;                                           // velocidad lineal
  CR = 0;                                              // contador ticks
  //  vectorR[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};        // vector de almacenamiento de datos para promedio del tiempo de interrupciones
  Rdistancia = 0;                                    // distancia recorrida llanta derecha
  Rtick = 0;                                           // ticks del encoder derecho
  RtickAnt = 0;                                        // ticks del encoder derecho anteriores
  deltaRtick = 0;                                      // diferencia del encoder derecho
  muestreoActualInterrupcionL = 0;       // variables para definición del tiempo de interrupción y cálculo de la velocidad motor Izquierdo
  muestreoAnteriorInterrupcionL = 0;
  deltaMuestreoInterrupcionL = 0;
  frecuenciaL = 0;                                  // frecuencia de interrupción llanta Izquierda
  Wl = 0;                                           // velocidad angular L
  Vl = 0;                                           // velocidad Lineal
  CL = 0;                                              // contador Ticks
  //  vectorL[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};        // vector de almacenamiento de datos para promedio del tiempo de interrupciones
  Ldistancia = 0;                                    // distancia recorrida llanta izquierda
  Ltick = 0;                                           // ticks del encoder izquierdo
  LtickAnt = 0;                                        // ticks del encoder izquier anteriores
  deltaLtick = 0;                                      // diferencia del encoder izquierdo
}
