// CREADO POR: Debora López
// MODIFICADO POR: Alvaro Torres
// FECHA DE CREACIÓN: 09/11/2021
// Control remoto para robot secador de café, mando de Debora López

//----------------------------------------------------------------------------------------------------
// LIBRERIAS INCLUIDAS
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA NRF24L01
#define CE 9
#define CSN 8    // PIN 8 DISPONIBLE

#define RESET 13
#define KA2284_BAT A8  // Pin analógico

#define MAX_eVERIFICACION 30
#define KA2284_ANALOG 150

// VALORES PARA ALMACENAR DATOS RECIBIDOS
#define AUTO_mode bitRead(direccionesCTRL,7)
#define start     bitRead(direccionesCTRL,6)
#define avanzar   bitRead(direccionesCTRL,5)
#define retroce   bitRead(direccionesCTRL,4)
#define izq       bitRead(direccionesCTRL,3)
#define dere      bitRead(direccionesCTRL,2)

RF24 radio(CE, CSN);                            //creamos el objeto radio (NRF24L01)
const byte addresses[][6] = {"00001", "00002"}; //Variable con la dirección del canal por donde se va a transmitir y leer

//----------------------------------------------------------------------------------------------------
// VALORES QUE RECIBO DEL CONTROL
struct Control_Data {
  byte INFO_PUSHS;
  byte INFO_ASPAS;
  byte VERIFICACION_C;
};

// VALORES QUE MANDO AL CONTROL
struct Robot_Data {
  byte Bateria_R;
  byte VERIFICACION_R;
  byte T_Operacion_Horas;
  byte T_Operacion_Min;
  byte INFO_ACTUAL1;
  byte INFO_ASPAS;
  byte Velocidad_robot;
  byte ALARMAS;
};

Control_Data data_control;
Robot_Data data_robot;


//----------------------------------------------------------------------------------------------------
// OTRAS VARIABLES
bool estado = false;

// MOVIMIENTO
byte direccionesCTRL;
byte velocidad_CTRL;
byte cont_e_Verificacion;
byte banderas;

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA QUE EL CARRO AVANCE //
#define E1  8       // PWM1 Speed Control Derecho              analogWrite (E1,#);
#define E2  9       // PWM2 Speed Control Izquierdo
#define M1  10      // EN1 Direction Control Derecho        digitalWrite(M1,HIGH/LOW);
#define M2  11      // EN2 Direction Control Izquierdo

// POSIBLES VELOCIDADES
#define VEL_MAX 220
#define VEL_MED 160
#define VEL_MIN 100

byte Velocidad = VEL_MIN;       // Velocidad del carrito

byte Velocidad_D = Velocidad;       // Velocidad inicial del motor derecho
byte Velocidad_I = Velocidad;       // Velocidad inicial del motor izquierdo
byte Velocidad_Min = Velocidad - 30;      // Velocidad mínima del carrito

unsigned long Tick_D = 0;
unsigned long Tick_I = 0;


//----------------------------------------------------------------------------------------------------
void setup() {
  // NRF24L01
  radio.begin();                           //inicializamos el NRF24L01
  radio.openWritingPipe(addresses[0]);     //Abrimos un canal de escritura (00001)
  radio.openReadingPipe(1, addresses[1]);  //Abrimos el canal de Lectura (00002)
  radio.setPALevel(RF24_PA_MIN);

  // Variables para promedio
  Serial.begin(115200);       // Inicialización de la comunicación serial

  // Variables para carrito
  pinMode(E1, OUTPUT);          // PIN de velocidad para el motor derecho como salida
  pinMode(E2, OUTPUT);          // PIN de velocidad para el motor izquierdo como salida
  pinMode(M1, OUTPUT);          // PIN de dirección del motor derecho como salida
  pinMode(M2, OUTPUT);          // PIN de dirección del motor izquierdo como salida
}

//----------------------------------------------------------------------------------------------------
void loop() {
  //    Recibir_NRF();
  //    imprimir();
  //    delay(500);

  //    while (AUTO_mode == true) {
  //      Serial.println("Modo automático: ON ");
  //      while  (AUTO_mode == 1) {
  //        Recibir_NRF();
  //      }
  //    }

  if (AUTO_mode == 0) {
    Serial.println("Modo manual: ON ");
    while  (AUTO_mode == 0) {
      Recibir_NRF();
      if (start == 1) {
        Recibir_NRF();
        Serial.println("ANDANDO");

        if (avanzar == true) {
          Velocidad_Min = Velocidad-30;
          
          avance(Velocidad_D, Velocidad_I);
          //  Serial.print(Tick_D);
          //  Serial.print(",");
          //  Serial.print(Tick_I);
          //  Serial.print(",");
          //  Serial.print(Velocidad_D);
          //  Serial.print(",");
          //  Serial.println(Velocidad_I);

          if (Tick_D > Tick_I) {
            Velocidad_D = Velocidad_D - 1;
          } else if (Tick_I  > Tick_D) {
            Velocidad_I = Velocidad_I - 1;
          } else {
            Velocidad_D = Velocidad;
            Velocidad_I = Velocidad;
          }

          if (Velocidad_D < Velocidad_Min) {
            Velocidad_D = Velocidad_Min;
          }
          if (Velocidad_I < Velocidad_Min) {
            Velocidad_I = Velocidad_Min;
          }

          Serial.println("Avanzando");
        }

        else if  (retroce == true) {
          retroceso(Velocidad, Velocidad);
          Serial.println("Retrocediendo");
        }

        else  if (izq == true) {
          izquierda(80, 80);
          Serial.println("Izquierda");
        }

        else  if (dere == true) {
          derecha(80, 80);
          Serial.println("Derecha");
        }

        else {
          parar();
          Serial.println("DETENIDO");
        }

      }
    }
    parar();
  }
}

//----------------------------------------------------------------------------------------------------
// Función para que el robot avance
void avance(char a, char b) {        // Moverse hacia adelante
  analogWrite (E1, a);
  digitalWrite(M1, LOW);
  analogWrite (E2, b);
  digitalWrite(M2, LOW);
}

//----------------------------------------------------------------------------------------------------
// Función para que el robot retroceda
void retroceso(char a, char b) {        // Moverse hacia adelante
  analogWrite (E1, a);
  digitalWrite(M1, HIGH);
  analogWrite (E2, b);
  digitalWrite(M2, HIGH);
}

//----------------------------------------------------------------------------------------------------
// Función para que el robot gire derecha
void derecha(char a, char b) {        // Moverse hacia adelante
  analogWrite (E1, a);
  digitalWrite(M1, LOW);
  analogWrite (E2, b);
  digitalWrite(M2, HIGH);
}
//----------------------------------------------------------------------------------------------------
// Función para que el robot gire izquierda
void izquierda(char a, char b) {        // Moverse hacia adelante
  analogWrite (E1, a);
  digitalWrite(M1, HIGH);
  analogWrite (E2, b);
  digitalWrite(M2, LOW);
}
//----------------------------------------------------------------------------------------------------
// Funcion para detener al robot
void parar() {
  analogWrite (E1, 0);
  digitalWrite(M1, LOW);
  analogWrite (E2, 0);
  digitalWrite(M2, LOW);
}

//----------------------------------------------------------------------------------------------------
// SI EL NRF DA ERRORES
void Err_VERIFICACION() {
  if (data_control.VERIFICACION_C == 0) {
    cont_e_Verificacion++;
  }
  else if (data_control.VERIFICACION_C == B11100111) {
    cont_e_Verificacion = 0;
    bitClear(data_robot.ALARMAS, 3);
  }
  if (cont_e_Verificacion == MAX_eVERIFICACION) {
    bitSet(data_robot.ALARMAS, 3);                                                        //Enviar Error vía NRF
  } else if (cont_e_Verificacion > MAX_eVERIFICACION) {
    Serial.println(F("!ERR COMUNICACION CTRL!"));
    parar();
    delay(3000);
    cont_e_Verificacion = 0;
    Serial.println(F(" Resetndo..."));
    delay(2000);
    digitalWrite(RESET, LOW);          //Parar y Resetear
  }
}

//----------------------------------------------------------------------------------------------------
// RECIBIR DATOS DEL NRF24L01
int Recibir_NRF() {
  data_control.VERIFICACION_C = 0;

  //Recibir datos NRF
  radio.startListening();
  if (radio.available()) {
    while (radio.available()) {
      radio.read(&data_control, sizeof(Control_Data));  //Leemos los datos y los guardamos en la variable Control_Data
    }
  }

  //Preparar Datos Info Pushs
  if (bitRead(data_control.INFO_PUSHS, 7)) {
    bitSet(direccionesCTRL, 7); //AUTO_mode
  } else {
    bitClear(direccionesCTRL, 7);
  }
  if (bitRead(data_control.INFO_PUSHS, 6)) {
    bitSet(direccionesCTRL, 6); //start
  } else {
    bitClear(direccionesCTRL, 6);
  }
  if (bitRead(data_control.INFO_PUSHS, 3)) {
    bitSet(direccionesCTRL, 5); //avanzar
  } else {
    bitClear(direccionesCTRL, 5);
  }
  if (bitRead(data_control.INFO_PUSHS, 2)) {
    bitSet(direccionesCTRL, 4); //retroceder
  } else {
    bitClear(direccionesCTRL, 4);
  }
  if (bitRead(data_control.INFO_PUSHS, 1)) {
    bitSet(direccionesCTRL, 3); //izquierda
  } else {
    bitClear(direccionesCTRL, 3);
  }
  if (bitRead(data_control.INFO_PUSHS, 0)) {
    bitSet(direccionesCTRL, 2); //derecha
  } else {
    bitClear(direccionesCTRL, 2);
  }

  // VERIFICACIÓN DE RECEPCIÓN DE DATOS
  //  Err_VERIFICACION();

  //  //Preparar Verificación
  //  Serial.print(F("Verificacion: "));
  //  Serial.println(data_control.VERIFICACION_C, BIN);               // Serial.print(F("  |Info Pushs: ")); Serial.print(data_control.INFO_PUSHS, BIN);
  //  data_robot.VERIFICACION_R = data_control.VERIFICACION_C;

  //Preparar Velocidades
  velocidad_CTRL = data_control.INFO_PUSHS & B00110000;
  velocidad_CTRL = velocidad_CTRL >> 4;

  switch (velocidad_CTRL) {
    case 1: Velocidad = VEL_MIN; break;
    case 2: Velocidad = VEL_MED; break;
    case 3: Velocidad = VEL_MAX; break;
    default: Velocidad = VEL_MIN; break;
  }
}

//----------------------------------------------------------------------------------------------------
// ENVIAR DATOS AL NRF
void Enviar_NRF() {
  delay(5);
  radio.stopListening();
  data_robot.Velocidad_robot = 0; // limpiar el byte

  // CARGO EL VALOR DE MI VELOCIDAD ACTUAL AL BYTE VELOCIDAD DE DATA ROBOT
  data_robot.Velocidad_robot = Velocidad;
  radio.write(&data_robot, sizeof(Robot_Data));
}

//----------------------------------------------------------------------------------------------------
// CÁLCULO DE PORCENTAJE DE BATERIA CON MÓDULO KA2284B
void KA2284() {                       //Lectura Batería
  if (analogRead(KA2284_BAT) > KA2284_ANALOG) {
    bitSet(data_robot.ALARMAS, 0);
  } else {
    bitClear(data_robot.ALARMAS, 0);
  }
}

//----------------------------------------------------------------------------------------------------
// FUNCIÓN PARA INCREMENTO DE TIKCS
void EncoderD () {
  Tick_D = Tick_D + 1;
  //    Serial.print("Tick Derecho: ");
  //    Serial.println(Tick_D);
}

void EncoderI () {
  Tick_I = Tick_I + 1;
  //    Serial.print("                 Tick Izquierdo: ");
  //    Serial.println(Tick_I);
}

//----------------------------------------------------------------------------------------------------
// FUNCIÓN PARA IMPRIMIR LO QUE RECIBO
void imprimir() {
  delay(100);
  Serial.println(data_control.INFO_PUSHS);
  Serial.println();
  Serial.print("Modo automático: ");
  Serial.println(AUTO_mode);
  Serial.print("Botton para iniciar: ");
  Serial.println(start);
  Serial.print("Avance: ");
  Serial.println(avanzar);
  Serial.print("Retroceso: ");
  Serial.println(retroce);
  Serial.print("Giro izquierda: ");
  Serial.println(izq);
  Serial.print("Giro derecha: ");
  Serial.println(dere);
  Serial.print("Velocidad: ");
  Serial.println(Velocidad, BIN);
}
