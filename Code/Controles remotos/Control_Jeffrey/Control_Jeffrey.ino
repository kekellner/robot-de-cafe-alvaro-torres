// CREADO POR: Jeffrey Muñoz
// MODIFICADO POR: Alvaro Torres
// FECHA DE CREACIÓN: 11/11/2021
// Control remoto para robot secador de café, mando de Jeffrey Muñoz

//----------------------------------------------------------------------------------------------------
// LIBRERIAS INCLUIDAS
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

//----------------------------------------------------------------------------------------------------
// VARIABLES NRF
RF24 radio(9, 8); // CE, CSN
const byte addresses[][6] = {"00001", "00002"};

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA SELECCIÓN DE MODO (AUTOMÁTICO - MANUAL)
int DATAC[10];
int modestate = 0;
int oldstate = 1;
int newstate;

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA QUE EL CARRO AVANCE //

#define E1  8       // PWM1 Speed Control Derecho              analogWrite (E1,#);
#define E2  9       // PWM2 Speed Control Izquierdo
#define M1  10      // EN1 Direction Control Derecho        digitalWrite(M1,HIGH/LOW);
#define M2  11      // EN2 Direction Control Izquierdo

byte lento = 100;           // Primera velocidad
byte medio = 160;           // Segunda velocidad
byte rapido = 220;          // Tercera velocidad
byte Velocidad = lento;       // Velocidad inicial del motor derecho

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA CONTROLADOR
#define encD  2       // Encoder derecho
#define encI  3       // Encoder Izquierdo

byte Velocidad_Min = Velocidad - 30;      // Velocidad mínima del carrito

byte Velocidad_D = Velocidad;       // Velocidad inicial del motor derecho
byte Velocidad_I = Velocidad;       // Velocidad inicial del motor izquierdo

unsigned long Tick_D = 0;
unsigned long Tick_I = 0;
//----------------------------------------------------------------------------------------------------
void setup() {
  Serial.begin(115200);

  // Variable para NRF
  radio.begin();
  radio.openWritingPipe(addresses[0]);        // 00001
  radio.openReadingPipe(1, addresses[1]);     // 00002
  radio.setPALevel(RF24_PA_MAX);

  // Variables para carrito
  pinMode(E1, OUTPUT);          // PIN de velocidad para el motor derecho como salida
  pinMode(E2, OUTPUT);          // PIN de velocidad para el motor izquierdo como salida
  pinMode(M1, OUTPUT);          // PIN de dirección del motor derecho como salida
  pinMode(M2, OUTPUT);          // PIN de dirección del motor izquierdo como salida

  // Linea para añadir una interrupciòn a un PIN
  attachInterrupt(digitalPinToInterrupt(encD), EncoderD, RISING);
  attachInterrupt(digitalPinToInterrupt(encI), EncoderI, RISING);


}

//----------------------------------------------------------------------------------------------------
void loop() {



  radio.startListening();

  if ( radio.available()) {
    while (radio.available()) {


      radio.read(DATAC, sizeof(DATAC));

      // -----------------------------------------
      // -------- Modo automático/Manual ---------
      //PREGUNTAR COMO FUNCIONA
      if (DATAC[4] == LOW && oldstate == 1) {
        if (modestate == 0) {
          modestate = 1;
          Serial.println("MODO AUTOMÁTICO");
        } else {
          modestate = 0;
          Serial.println("MODO MANUAL");
        }
      }

      oldstate = DATAC[4];

      // -----------------------------------------
      // ---------- DIRECCIÓN DEL CARRO ----------
      if (DATAC[0] == HIGH) {
        derecha(80, 80);

        Serial.print("Cruzando derecha ");
        Serial.println("Velocidad: 80 ");



      }

      else if (DATAC[1] == HIGH) {
        izquierda(80, 80);

        Serial.print("Cruzando izquierda ");
        Serial.println("Velocidad: 80");
      }

      else if (DATAC[2] == HIGH) {

        retroceder(Velocidad, Velocidad);

        Serial.print("Retrocediendo ");
        Serial.print("Velocidad: ");
        Serial.println(Velocidad);
      }



      else if (DATAC[3] == HIGH) {

        if (Tick_D > Tick_I) {
          Velocidad_D = Velocidad_D - 1;
        } else if (Tick_I  > Tick_D) {
          Velocidad_I = Velocidad_I - 1;
        } else {
          Velocidad_D = Velocidad;
          Velocidad_I = Velocidad;
        }

        if (Velocidad_D < Velocidad_Min) {
          Velocidad_D = Velocidad_Min;
        }
        if (Velocidad_I < Velocidad_Min) {
          Velocidad_I = Velocidad_Min;
        }

        avanzar(Velocidad_D, Velocidad_I);

        Serial.print("Avanzando ");
        Serial.print("Velocidad: ");
        Serial.println(Velocidad);
        Serial.print(Tick_D);
        Serial.print(",");
        Serial.print(Tick_I);
        Serial.print(",");
        Serial.print(Velocidad_D);
        Serial.print(",");
        Serial.println(Velocidad_I);

      }
      if ( DATAC[0] == HIGH & DATAC[1] == HIGH & DATAC[2] == HIGH) {
        Tick_I = 0;
        Tick_D = 0;
        Velocidad_I = Velocidad;
        Velocidad_D = Velocidad;
      }
      else {
        parar();
        Serial.print("PARADO ");
        Serial.println("Velocidad: 0");
      }

      // -----------------------------------------
      // ---------- VELOCIDAD DEL CARRO ----------
      if (DATAC[5] == 1) {
        Velocidad = lento;
      }

      else if (DATAC[6] == 1) {
        Velocidad = medio;
      }

      else if (DATAC[7] == 1) {
        Velocidad = rapido;
      }
      // -----------------------------------------

    }
    delay(5);

  }
}


//----------------------------------------------------------------------------------------------------
// Función para avanzar
void avanzar(char a, char b) {
  analogWrite (E1, a);
  digitalWrite(M1, LOW);
  analogWrite (E2, b);
  digitalWrite(M2, LOW);
}

// Función para girar izquierda
void izquierda (char a, char b) {
  analogWrite (E1, a);
  digitalWrite(M1, HIGH);
  analogWrite (E2, b);
  digitalWrite(M2, LOW);
}

// Función para girar derecha
void derecha (char a, char b) {
  analogWrite (E1, a);
  digitalWrite(M1, LOW);
  analogWrite (E2, b);
  digitalWrite(M2, HIGH);
}

// Función para retroceder
void retroceder (char a, char b) {
  analogWrite (E1, a);
  digitalWrite(M1, HIGH);
  analogWrite (E2, b);
  digitalWrite(M2, HIGH);
}

// Función para parar
void parar() {
  analogWrite (E1, 0);
  digitalWrite(M1, LOW);
  analogWrite (E2, 0);
  digitalWrite(M2, LOW);
}

// Función para encoder derecho
void EncoderD () {
  Tick_D = Tick_D + 1;
  //    Serial.print("Tick Derecho: ");
  //    Serial.println(Tick_D);
}

// Función para encoder izquierdo
void EncoderI () {
  Tick_I = Tick_I + 1;
  //    Serial.print("                 Tick Izquierdo: ");
  //    Serial.println(Tick_I);
}
