// AUTOR: Alvaro Javier Torres González
// ULTIMA MODIFICACIÓN: 16/11/2021
// CONTROL AUTOMÁTICO DEL ROBOT SECADOR DE CAFÉ
//    UTILIZANDO DWM1001
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA PROMEDIO
long X;       // Variable para valor de posición X
long Y;       // Variable para valor de posición Y
long Z;       // Variable para valor de posición Z
byte Q;       // Variable para valor de Calidad

const int N = 200;        // Número de datos para sacar el promedio

byte lectura0x40 = 0;       // Verificación para tipo de dato
byte lectura0x01 = 0;       // Verificación para tamaño de dato
byte lectura0x00 = 0;       // Verificación para error
byte lectura0x41 = 0;       // Verificación para tipo de dato 
byte lectura0x0d = 0;       // Verificación para tamaño de dato

byte bufer[13];       // Buffer de lectura del posición del DWM

long VectorX[N];        // Vector para sacar el promedio de la posición X
long VectorY[N];        // Vector para sacar el promedio de la posición Y
long VectorZ[N];        // Vector para sacar el promedio de la posición Z
long VectorQ[N];        // Vector para sacar el promedio de la Calidad

// -----------------------------------------------------
// VARIABLES PARA QUE EL CARRO AVANCE //
#define encD  2       // Encoder derecho
#define encI  3       // Encoder Izquierdo

#define E1  8       // PWM1 Speed Control Derecho              analogWrite (E1,#);
#define E2  9       // PWM2 Speed Control Izquierdo
#define M1  10      // EN1 Direction Control Derecho        digitalWrite(M1,HIGH/LOW);
#define M2  11      // EN2 Direction Control Izquierdo

byte Velocidad = 80;       // Velocidad del carrito
byte Velocidad_Max = Velocidad + 25;     // Velocidad máxima del carrito
byte Velocidad_Min = Velocidad - 25;      // Velocidad mínima del carrito

int LineaX = 400;                   // Valor en mm de la linea que queremos seguir
int Limite_InfX = LineaX - 100;     // Limite inferior en mm de linea
int Limite_SupX = LineaX + 100;     // Limite superior en mm de linea

byte Velocidad_D = Velocidad;       // Velocidad inicial del motor derecho
byte Velocidad_I = Velocidad;       // Velocidad inicial del motor izquierdo

// -----------------------------------------------------
// OTRAS VARIABLES
bool estado = LOW;
#define Push 22
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
void setup() {
  // Comunicación serial
  Serial1.begin(115200);
  Serial.begin(115200);

  // Variables para promedio
  for (int z = 0; z < N; z++)  {        // Iniciamos los vectores X, Y, Z, Q en cero
    VectorX[z] = 0;
    VectorY[z] = 0;
    VectorZ[z] = 0;
    VectorQ[z] = 0;
  }

  // Variables para carrito
  pinMode(E1, OUTPUT);          // PIN de velocidad para el motor derecho como salida
  pinMode(E2, OUTPUT);          // PIN de velocidad para el motor izquierdo como salida
  pinMode(M1, OUTPUT);          // PIN de dirección del motor derecho como salida
  pinMode(M2, OUTPUT);          // PIN de dirección del motor izquierdo como salida

  // Otras variables
  pinMode(Push, INPUT);
}
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
void loop() {
  Serial1.write(2);
  Serial1.write(0);
  delay (5);

  // -----------------------------------------------------
  // ----- Posición recibida DWM -----
  if (Serial1.available()) {
    lectura0x40 = Serial1.read();

    if (lectura0x40 == 0x40) {
      while (!Serial1.available()) {}
      lectura0x01 = Serial1.read();

      if (lectura0x01 == 0x01) {
        while (!Serial1.available()) {}
        lectura0x00 = Serial1.read();

        if (lectura0x00 == 0x00) {
          while (!Serial1.available()) {}
          lectura0x41 = Serial1.read();

          if (lectura0x41 == 0x41) {
            while (!Serial1.available()) {}
            lectura0x0d = Serial1.read();

            if (lectura0x0d == 0x0d) {
              Serial1.readBytes(bufer, 13);
            }
          }
        }
      }
    }

    // ----- Formando valor X,Y,Z y QL -----
    X = (bufer[3] << 24) + (bufer[2] << 16) + (bufer[1] << 8) + (bufer[0]);
    Y = (bufer[7] << 24) + (bufer[6] << 16) + (bufer[5] << 8) + (bufer[4]);
    Z = (bufer[11] << 24) + (bufer[10] << 16) + (bufer[9] << 8) + (bufer[8]);
    Q = bufer[12];

    // ----- Validando valor recibido -----
    if (X > Limite_InfX && X < Limite_SupX) {
      promedio(X, 1);
      promedio(Y, 2);
      promedio(Z, 3);
      promedio(Q, 4);
                    Serial.print ("Valor X: ");
                    Serial.print (X);
                    Serial.print("  - Valor agregado");
                    Serial.print("  - Promedio: ");
      Serial.println(promedio(X, 1));
    } else {
                  Serial.print ("Valor X: ");
                  Serial.print (X);
      Serial.println("  - Rechazado");
    }
  }
  // -----------------------------------------------------

  //  Serial.print(X);
  //  Serial.print(",");
  //  Serial.print(promedio(X, 1), DEC);
  //  Serial.print(",");
  //  Serial.print(Y);
  //  Serial.print(",");
  //  Serial.print(promedio(Y, 2), DEC);
  //  Serial.print(",");
  //  Serial.print(Z);
  //  Serial.print(",");
  //  Serial.println(promedio(Z, 3), DEC);

  // -----------------------------------------------------
  // ----------- Botón de arranque -----------
  if (digitalRead(Push) == HIGH) {
    while (digitalRead(Push) == HIGH) {}
    estado = HIGH;
  }
  // -----------------------------------------------------

  // -----------------------------------------------------
  // ----- Hacer cuando se presiona el boton -----
  while (estado == HIGH) {
    Serial1.write(2);
    Serial1.write(0);
    delay (5);

    // --------- Posición recibida DWM ---------
    if (Serial1.available()) {
      lectura64 = Serial1.read();

      if (lectura64 == 0x40) {
        while (!Serial1.available()) {}
        lectura1 = Serial1.read();

        if (lectura1 == 0x01) {
          while (!Serial1.available()) {}
          lectura0 = Serial1.read();

          if (lectura0 == 0x00) {
            while (!Serial1.available()) {}
            lectura65 = Serial1.read();

            if (lectura65 == 0x41) {
              while (!Serial1.available()) {}
              lectura13 = Serial1.read();

              if (lectura13 == 0x0D) {
                Serial1.readBytes(bufer, 13);
              }
            }
          }
        }
      }

      // ------ Formando valor X,Y,Z y QL ------
      X = (bufer[3] << 24) + (bufer[2] << 16) + (bufer[1] << 8) + (bufer[0]);
      Y = (bufer[7] << 24) + (bufer[6] << 16) + (bufer[5] << 8) + (bufer[4]);
      Z = (bufer[11] << 24) + (bufer[10] << 16) + (bufer[9] << 8) + (bufer[8]);
      Q = bufer[12];

      // ------ Validando valor recibido ------
      if (X > Limite_InfX && X < Limite_SupX) {
        promedio(X, 1);
        promedio(Y, 2);
        promedio(Z, 3);
        promedio(Q, 4);
        Velocidad_D = Velocidad;
        Velocidad_I = Velocidad;
    }

    // ----- Mandando velocidad a los motores -----
    avanzar(Velocidad_D, Velocidad_I);

    // ----- Ambos motores a la misma velocidad -----
    if (Velocidad_D == Velocidad_Min && Velocidad_I == Velocidad_Min) {
      Velocidad_D = Velocidad;
      Velocidad_I = Velocidad;
    }

    // ----- Control de velocidad -----
    if (promedio(X, 1) > Limite_SupX) {           // Si se detecta que el robot superó el límite superior
      Velocidad_I = Velocidad_I - 1;              // Se disminuye la velocidad del motor opuesto
    } else if (promedio(X, 1) < Limite_InfX) {    // Si se detecta que el robot superó el límite inferior
      Velocidad_D = Velocidad_D - 1;              // Se disminuye la velocidad del motor opuesto
    } else {                                      // Si el robot se encuentra encarrilado
      Velocidad_D = Velocidad;                    // Ambos motores van a la misma velocidad.
      Velocidad_I = Velocidad;
    }

    // ----- Límite de velocidad mínima -----
    if (Velocidad_I < Velocidad_Min) {
      Velocidad_I = Velocidad_Min;
    }
    if (Velocidad_D < Velocidad_Min) {
      Velocidad_D = Velocidad_Min;
    }
  }
}
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
// -----------------------------------------------------
// FUNCION PARA CÁLCULO DE PROMEDIO
long promedio(long Valor, int val) {
  long sum = 0;
  long Vector1[N];

  // ----- Selector de coordenada X, Y, Z o Q -----
  switch (val) {
    case 1:
      for (int z = 0; z < N; z++) {
        Vector1[z] = VectorX[z];
      } break;
    case 2:
      for (int z = 0; z < N; z++) {
        Vector1[z] = VectorY[z];
      } break;
    case 3:
      for (int z = 0; z < N; z++) {
        Vector1[z] = VectorZ[z];
      } break;
    case 4:
      for (int z = 0; z < N; z++) {
        Vector1[z] = VectorQ[z];
      } break;
  }

  // ----- Coorimiento y agregando nuevo valor -----
  for (int i = 0; i < N; i++) {
    Vector1[i] = Vector1[i + 1];
  }
  Vector1[N - 1] = Valor;
  for (int i = 0; i < N; i++) {
    sum = sum + Vector1[i];
  }

  // ----- Guardandolo la nueva matriz primedio -----
  switch (val) {
    case 1:
      for (int z = 0; z < N; z++) {
        VectorX[z] = Vector1[z];
      } break;
    case 2:
      for (int z = 0; z < N; z++) {
        VectorY[z] = Vector1[z];
      } break;
    case 3:
      for (int z = 0; z < N; z++) {
        VectorZ[z] = Vector1[z];
      } break;
    case 4:
      for (int z = 0; z < N; z++) {
        VectorQ[z] = Vector1[z];
      } break;
  }
  return sum / N;
}

// -----------------------------------------------------
// FUNCIÓN PARA AVANZAR
void avanzar(char a, char b) {
  analogWrite (E1, a);
  //  digitalWrite(M1, HIGH);
  analogWrite (E2, b);
  //  digitalWrite(M2, HIGH);
}
