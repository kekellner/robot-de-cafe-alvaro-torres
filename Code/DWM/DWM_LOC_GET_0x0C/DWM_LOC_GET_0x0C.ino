// AUTOR: Alvaro Javier Torres González
// ULTIMA MODIFICACIÓN: 24/11/2021
// CONTROL AUTOMÁTICO DEL ROBOT SECADOR DE CAFÉ
//    UTILIZANDO DWM1001 código 12 00
//    sección 5.3.10 API
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
// VARIABLES PARA LECTURA DEL DWM CON COMANDO 12 00
// Bytes que se reciben - pag. 54 documento API
// 0x40 0x01 0x00 0x41 0x0D (position 13bytes = 0x##) 0x49 0x3D 0X03
// ((A3 - 581A)0X1A 0X58) (distance 4bytes = 0x##) (Ql = 0x64) (position 13bytes = 0x##)
// ((A1 - 1D25)0X25 0X1d) (distance 4bytes = 0x##) (Ql = 0x64) (position 13bytes = 0x##)
// ((A2 - 4BB8)0Xb8 0X4b) (distance 4bytes = 0x##) (Ql = 0x64) (position 13bytes = 0x##)

// -----------------------------------------------------
// VARIABLES PARA DEFINIR POSICIÓN FÍSICA DE LOS ANCHORS
float PosA1 [2] = {0.00, 0.00};
float PosA2 [2] = {0.60, 0.00};
float PosA3 [2] = {0.60, 0.60};

// -----------------------------------------------------
// VALORES DE VERIFICACIÓN A PARTIR DEL BYTE 19 RECIBIDO
byte lectura0x49 = 0;       // Verificación para tipo de dato
byte lectura0x3d = 0;       // Verificación para tamaño de dato
byte lectura0x03 = 0;       // Verificación para cantidad de anchors

byte lectura0xb8 = 0;       // Verificación para byte 1 - anchor1
byte lectura0x4b = 0;       // Verificación para byte 2 - anchor1

byte lectura0x25 = 0;       // Verificación para byte 1 - anchor2
byte lectura0x1d = 0;       // Verificación para byte 2 - anchor2

byte lectura0x1a = 0;       // Verificación para byte 1 - anchor3
byte lectura0x58 = 0;       // Verificación para byte 2 - anchor3

byte bufer[60];       // Buffer completo 3 anchors


// -----------------------------------------------------
// VARIABLES PARA GUARDAR ANCHORS Y DISTANCIA DE LOS ANCHORS
long ANC1;        // Variable para anchor 1 leido
long ANC2;        // Variable para anchor 2 leido
long ANC3;        // Variable para anchor 3 leido

float Dist1;       // Variable para distancia 1 leida
float Dist2;       // Variable para distancia 2 leida
float Dist3;       // Variable para distancia 3 leida

float X1;       // Variable para valor de posición X
float Y1;       // Variable para valor de posición Y
float X2;       // Variable para valor de posición X
float Y2;       // Variable para valor de posición Y
float X3;       // Variable para valor de posición X
float Y3;       // Variable para valor de posición Y

byte Q1;       // Variable para valor de Calidad
byte Q2;       // Variable para valor de Calidad
byte Q3;       // Variable para valor de Calidad

float a;
float b;
float c;

float An1 = 0;
float Bn1 = 0;
float Cn1;

// -----------------------------------------------------
// OTRAS VARIABLES
bool estado1 = LOW;
bool estado2 = LOW;

#define Push 22
#define pi 3.14159265359
#include <math.h>

unsigned long MillisActual = 0;
unsigned long MillisAnterior = 0;
const long ValorMillis = 2000;


//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
void setup() {
  // Comunicación serial
  Serial1.begin(115200);
  Serial.begin(9600);

  // Otras variables
  pinMode(Push, INPUT);
}
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
void loop() {
  if (digitalRead(Push) == HIGH) {
    while (digitalRead(Push) == HIGH) {}
    MillisActual = millis();
    MillisAnterior = millis();
    Serial1.write(12);
    Serial1.write(0);
    estado1 = HIGH;
  }

  while (estado1 == HIGH) {
    while ( (MillisActual - MillisAnterior) >= ValorMillis) {
//      for (int N = 0; N < 50; N++)  {
//        Serial.println();
//        Serial.println("ENTRÓ A FUNCIÓN MILLIS");
//      }

      MillisAnterior = MillisActual;
      Serial1.write(12);
      Serial1.write(0);
    }


    MillisActual = millis();
    LecturaDeDatos();


  }
}

//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
// -----------------------------------------------------
// FUNCIÓN PARA VALIDAR Y LEER LOS DATOS DE RTLS
void LecturaDeDatos() {
  if (Serial1.available()) {
    lectura0x49 = Serial1.read();
    if (lectura0x49 == 0x49) {
      while (!Serial1.available()) {}
      lectura0x3d = Serial1.read();
      if (lectura0x3d == 0x3d) {
        while (!Serial1.available()) {}
        lectura0x03 = Serial1.read();
        if (lectura0x03 == 0x03) {
          while (!Serial1.available()) {}
          Serial1.readBytes(bufer, 60);
          estado2 = true;
        }
      }
    }

    if (estado2 == true) {
      FormarValores();
      OrdenarValores();
      PosicionTAG();
      ImprimirValores ();
      MillisAnterior = MillisActual;
      estado2 = false;

      Serial1.write(12);
      Serial1.write(0);
    }
  }
}

// -----------------------------------------------------
// FUNCIÓN PARA FORMAR VALORES RECIBIDOS
void FormarValores() {
  // ----- Formando anchor1 y distancia 1 -----
  ANC1 = (bufer[1] << 8) + (bufer[0]);
  Dist1 = (bufer[5] << 24) + (bufer[4] << 16) + (bufer[3] << 8) + (bufer[2]);
  //  Q1 = bufer[6];
  //  X1 = (bufer[10] << 24) + (bufer[9] << 16) + (bufer[8] << 8) + (bufer[7]);
  //  Y1 = (bufer[14] << 24) + (bufer[13] << 16) + (bufer[12] << 8) + (bufer[11]);
  //  Z1 = (bufer[18] << 24) + (bufer[17] << 16) + (bufer[16] << 8) + (bufer[15]);
  //  Q1 = bufer[19];

  // ----- Formando anchor1 y distancia 2 -----
  ANC2 = (bufer[21] << 8) + (bufer[20]);
  Dist2 = (bufer[25] << 24) + (bufer[24] << 16) + (bufer[23] << 8) + (bufer[22]);
  //  Q2 = bufer[26];
  //  X2 = (bufer[30] << 24) + (bufer[29] << 16) + (bufer[28] << 8) + (bufer[27]);
  //  Y2 = (bufer[34] << 24) + (bufer[33] << 16) + (bufer[32] << 8) + (bufer[31]);
  //  Z2 = (bufer[38] << 24) + (bufer[37] << 16) + (bufer[36] << 8) + (bufer[35]);
  //  Q2 = bufer[39];

  // ----- Formando anchor1 y distancia 3 -----
  ANC3 = (bufer[41] << 8) + (bufer[40]);
  Dist3 = (bufer[45] << 24) + (bufer[44] << 16) + (bufer[43] << 8) + (bufer[42]);
  //  Q3 = bufer[46];
  //  X3 = (bufer[50] << 24) + (bufer[49] << 16) + (bufer[48] << 8) + (bufer[47]);
  //  Y3 = (bufer[54] << 24) + (bufer[53] << 16) + (bufer[52] << 8) + (bufer[51]);
  //  Z3 = (bufer[58] << 24) + (bufer[57] << 16) + (bufer[56] << 8) + (bufer[55]);
  //  Q3 = bufer[59];
}

// -----------------------------------------------------
// FUNCIÓN PARA ORDENAR VALORES RECIBIDOS
void OrdenarValores() {
  if (ANC1 == 7461) {       // (A1 - 1D25)
    if (ANC2 == 19384) {    // (A2 - 4BB8)
      long A1 = ANC1;
      long A2 = ANC2;
      long A3 = ANC3;
      float Dis1 = Dist1;
      float Dis2 = Dist2;
      float Dis3 = Dist3;
      ANC1 = A1;
      ANC2 = A2;
      ANC3 = A3;
      Dist1 = Dis1 / 1000;
      Dist2 = Dis2 / 1000;
      Dist3 = Dis3 / 1000;
    } else if (ANC2 == 22554) {   // (A3 - 581A)
      long A1 = ANC1;
      long A2 = ANC3;
      long A3 = ANC2;
      float Dis1 = Dist1;
      float Dis2 = Dist3;
      float Dis3 = Dist2;
      ANC1 = A1;
      ANC2 = A2;
      ANC3 = A3;
      Dist1 = Dis1 / 1000;
      Dist2 = Dis2 / 1000;
      Dist3 = Dis3 / 1000;
    }
  }

  else if (ANC1 == 19384) {    // (A2 - 4BB8)
    if (ANC2 == 7461) {   // (A1 - 1D25)
      long A1 = ANC2;
      long A2 = ANC1;
      long A3 = ANC3;
      float Dis1 = Dist2;
      float Dis2 = Dist1;
      float Dis3 = Dist3;
      ANC1 = A1;
      ANC2 = A2;
      ANC3 = A3;
      Dist1 = Dis1 / 1000;
      Dist2 = Dis2 / 1000;
      Dist3 = Dis3 / 1000;
    } else if (ANC2 == 22554) {   // (A3 - 581A)
      long A1 = ANC2;
      long A2 = ANC3;
      long A3 = ANC1;
      float Dis1 = Dist2;
      float Dis2 = Dist3;
      float Dis3 = Dist1;
      ANC1 = A1;
      ANC2 = A2;
      ANC3 = A3;
      Dist1 = Dis1 / 1000;
      Dist2 = Dis2 / 1000;
      Dist3 = Dis3 / 1000;
    }
  }

  else if (ANC1 == 22554) {    // (A3 - 581A)
    if (ANC2 == 7461) {   // (A1 - 1D25)
      long A1 = ANC3;
      long A2 = ANC1;
      long A3 = ANC2;
      float Dis1 = Dist3;
      float Dis2 = Dist1;
      float Dis3 = Dist2;
      ANC1 = A1;
      ANC2 = A2;
      ANC3 = A3;
      Dist1 = Dis1 / 1000;
      Dist2 = Dis2 / 1000;
      Dist3 = Dis3 / 1000;
    } else if (ANC2 == 19384) {   // (A2 - 4BB8)
      long A1 = ANC3;
      long A2 = ANC2;
      long A3 = ANC1;
      float Dis1 = Dist3;
      float Dis2 = Dist2;
      float Dis3 = Dist1;
      ANC1 = A1;
      ANC2 = A2;
      ANC3 = A3;
      Dist1 = Dis1 / 1000;
      Dist2 = Dis2 / 1000;
      Dist3 = Dis3 / 1000;
    }
  }
}

// -----------------------------------------------------
// FUNCIÓN PARA FORMAR POSICIÓN DEL TAG
void PosicionTAG() {
  a = sqrt(pow((PosA3[0] - PosA2[0]), 2) + pow(PosA3[1] - PosA2[1], 2));
  b = sqrt(pow((PosA3[0] - PosA1[0]), 2) + pow(PosA3[1] - PosA1[1], 2));
  c = sqrt(pow((PosA2[0] - PosA1[0]), 2) + pow(PosA2[1] - PosA1[1], 2));

  An1 = acos((-pow(Dist2, 2) + pow(c, 2) + pow(Dist1, 2)) / (2 * c * Dist1)) * (180 / pi);
  float An2 = acos((-pow(Dist3, 2) + pow(b, 2) + pow(Dist1, 2)) / (2 * b * Dist1)) * (180 / pi);


  Bn1 = acos((-pow(Dist1, 2) + pow(c, 2) + pow(Dist2, 2)) / (2 * c * Dist2)) * (180 / pi);
  float Bn2 = acos((-pow(Dist3, 2) + pow(a, 2) + pow(Dist2, 2)) / (2 * a * Dist2)) * (180 / pi);

  Cn1 = acos((-pow(Dist1, 2) + pow(b, 2) + pow(Dist3, 2)) / (2 * b * Dist3)) * (180 / pi);
  float Cn2 = acos((-pow(Dist2, 2) + pow(a, 2) + pow(Dist3, 2)) / (2 * a * Dist3)) * (180 / pi);

  X1 = Dist1 * cos(An1 * pi / 180);
  Y1 = Dist1 * sin(An1 * pi / 180);

  X2 = (PosA2[0] - PosA1[0]) - (Dist2 * cos(Bn1 * pi / 180));
  Y2 = Dist2 * sin(Bn1 * pi / 180);

  X3 = (PosA3[0] - PosA1[0]) - (Dist3 * sin(Cn2 * pi / 180));
  Y3 = (PosA3[1] - PosA2[1]) - (Dist3 * cos(Cn2 * pi / 180));
}

// -----------------------------------------------------
// FUNCION PARA IMPRIMIR VALORES RECIBIDOS
void ImprimirValores () {
//  Serial.println();
//  Serial.print("Anchor 1 - 1D25: "); Serial.println(ANC1, HEX);
//  Serial.print("Distancia1: "); Serial.println(Dist1);
//  //  Serial.print("a: "); Serial.println(a);
//  Serial.print("An1: "); Serial.println(An1);
//  Serial.print("Posición: "); Serial.print(X1); Serial.print(" , "); Serial.println(Y1);
//  //  Serial.print("QL1: "); Serial.println(Q1, DEC);
//
//  Serial.print("Anchor 2 - 4BB8: "); Serial.println(ANC2, HEX);
//  Serial.print("Distancia2: "); Serial.println(Dist2);
//  //  Serial.print("b: "); Serial.println(b);
//  Serial.print("Bn1: "); Serial.println(Bn1);
//  Serial.print("Posición: "); Serial.print(X2); Serial.print(" , "); Serial.println(Y2);
//  //  Serial.print("QL2: "); Serial.println(Q2, DEC);
//
//  Serial.print("Anchor 3 - 581A: "); Serial.println(ANC3, HEX);
//  Serial.print("Distancia3: "); Serial.println(Dist3);
//  //  Serial.print("c: ");  Serial.println(c);
//  Serial.print("Cn1: "); Serial.println(Cn1);
//  Serial.print("Posición: "); Serial.print(X3); Serial.print(" , "); Serial.println(Y3);
//  //  Serial.print("QL3: "); Serial.println(Q3, DEC);

  Serial.println();
  Serial.print("Distancia1: "); Serial.println(Dist1);
  Serial.print("Distancia2: "); Serial.println(Dist2);
  Serial.print("Distancia3: "); Serial.println(Dist3);
  Serial.print("Posición: "); Serial.print(X1); Serial.print(" , "); Serial.println(Y1);
  Serial.print("Posición: "); Serial.print(X2); Serial.print(" , "); Serial.println(Y2);
  Serial.print("Posición: "); Serial.print(X3); Serial.print(" , "); Serial.println(Y3);
}
