// CREADO POR: Alvaro Torres
// FECHA DE CREACIÓN: 08/12/2021
// Control remoto para robot secador de café, pruebas de campo
//----------------------------------------------------------------------------------------------------
//-----------------------------------------------
// LIBRERIAS INCLUIDAS
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

//-----------------------------------------------
// VARIABLES PARA NRF24L01
#define CE 49
#define CSN 48    // PIN 8 DISPONIBLE

RF24 radio(CE, CSN);                            //creamos el objeto radio (NRF24L01)
const byte addresses[][6] = {"00001", "00002"}; //Variable con la dirección del canal por donde se va a transmitir y leer

//-----------------------------------------------
// VALORES QUE RECIBO DEL CONTROL
int Vector[5];
byte CONF_I;
byte Velocidad_MI;
byte Velocidad_MD;
byte INFO_ASPAS;
byte CONF_F;

//-----------------------------------------------
// VARIABLES PARA MOTORES DE ASPAS
#define As1 14
#define As2 15
#define As3 17
#define As4 16

//-----------------------------------------------
// VARIABLES PARA QUE EL CARRO AVANCE //
#define E1  8       // PWM1 Speed Control Derecho              analogWrite (E1,#);
#define E2  9       // PWM2 Speed Control Izquierdo
#define M1  10      // EN1 Direction Control Derecho        digitalWrite(M1,HIGH/LOW);
#define M2  11      // EN2 Direction Control Izquierdo

//-----------------------------------------------
// OTRAS VARIABLES


//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
void setup() {
  // NRF24L01
  radio.begin();                           //inicializamos el NRF24L01
  radio.openWritingPipe(addresses[0]);     //Abrimos un canal de escritura (00001)
  radio.openReadingPipe(1, addresses[1]);  //Abrimos el canal de Lectura (00002)
  radio.setPALevel(RF24_PA_MIN);

  // Variables para promedio
  Serial.begin(115200);         // Inicialización de la comunicación serial

  // Variables para carrito
  pinMode(E1, OUTPUT);          // PIN de velocidad para el motor derecho como salida
  pinMode(E2, OUTPUT);          // PIN de velocidad para el motor izquierdo como salida
  pinMode(M1, OUTPUT);          // PIN de dirección del motor derecho como salida
  pinMode(M2, OUTPUT);          // PIN de dirección del motor izquierdo como salida

  // Variables para motores de aspas
  digitalWrite(As1, LOW);          // Variables para controlar aspas con módulo L293D
  digitalWrite(As2, LOW);          // Variables para controlar aspas con módulo L293D
  digitalWrite(As3, LOW);          // Variables para controlar aspas con módulo L293D
  digitalWrite(As4, LOW);          // Variables para controlar aspas con módulo L293D
}
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
void loop() {
  Recibir_NRF();
  if (CONF_I == '<' && CONF_F == '>') {

    int D_velocidad = Velocidad_MD;
    int I_velocidad = Velocidad_MI;
    int V_Aspas = INFO_ASPAS;

    Movimiento (D_velocidad, 1);
    Movimiento (I_velocidad, 2);
    F_Aspas (V_Aspas);
  }
}


//----------------------------------------------------------------------------------------------------
//-----------------------------------------------
// RECIBIR DATOS DEL NRF24L01
int Recibir_NRF() {

  //Recibir datos NRF
  radio.startListening();
  if (radio.available()) {
    while (radio.available()) {
      radio.read(Vector, sizeof(Vector));
    }
  }

  CONF_I = Vector[0];
  Velocidad_MI = Vector[1];
  Velocidad_MD = Vector[2];
  INFO_ASPAS = Vector[3];
  CONF_F = Vector[4];

}


//-----------------------------------------------
// Función para que se muevan las ruedas
void Movimiento(byte a, byte b) {                                             // a = Velocidad (0 - 20) ; b = Motor (1 (derecho) ó 2 (izquierdo))
  bool Direccion;
  byte E_motor;
  byte M_motor;
  byte Velocidad;

  // Selección de velocidad de la rueda
  switch (a) {
    case 0: Velocidad = 255; break;
    case 1: Velocidad = 230; break;
    case 2: Velocidad = 205; break;
    case 3: Velocidad = 180; break;
    case 4: Velocidad = 155; break;
    case 5: Velocidad = 130; break;
    case 6: Velocidad = 105; break;
    case 7: Velocidad = 80; break;
    case 8: Velocidad = 55; break;
    case 9: Velocidad = 30; break;
    case 10: Velocidad = 0; break;
    case 11: Velocidad = 30; break;
    case 12: Velocidad = 55; break;
    case 13: Velocidad = 80; break;
    case 14: Velocidad = 105; break;
    case 15: Velocidad = 130; break;
    case 16: Velocidad = 155; break;
    case 17: Velocidad = 180; break;
    case 18: Velocidad = 205; break;
    case 19: Velocidad = 230; break;
    case 20: Velocidad = 255; break;

    default: Velocidad = 0; break;
      return;
  }

  // Selección de dirección del motor
  if (a < 10) {             // Movimiento hacia atras
    Direccion = HIGH;
  } else if (a > 10) {      // Movimiento hacia adelante
    Direccion = LOW;
  } else {                  // Si no se recibe valor
    Direccion = HIGH;
  }

  // Selección de motor a mover
  if (b == 1) {             // Motor derecho
    E_motor = E1;
    M_motor = M1;
  } else if (b == 2) {      // Motor Izquierdo
    E_motor = E2;
    M_motor = M2;
  }

  digitalWrite(M_motor, Direccion);
  analogWrite (E_motor, Velocidad);

}


//-----------------------------------------------
// FUNCIÓN ASPAS
void F_Aspas (int a) {
  switch (a) {

    case 0:
      digitalWrite(As1, LOW);
      digitalWrite(As2, HIGH);
      digitalWrite(As4, LOW);
      digitalWrite(As3, HIGH);
      Serial.println("Aspas adelante");
      break;

    case 1:
      digitalWrite(As1, LOW);
      digitalWrite(As2, LOW);
      digitalWrite(As4, LOW);
      digitalWrite(As3, LOW);
      Serial.println("Aspas parado");
      break;

    case 2:
      digitalWrite(As1, HIGH);
      digitalWrite(As2, LOW);
      digitalWrite(As4, HIGH);
      digitalWrite(As3, LOW);
      Serial.println("Aspas atras");
      break;

    default:
      digitalWrite(As1, LOW);
      digitalWrite(As2, LOW);
      digitalWrite(As4, LOW);
      digitalWrite(As3, LOW);
      Serial.println("Aspas default");
      break;
  }
}


//----------------------------------------------------------------------------------------------------
// FUNCIÓN PARA IMPRIMIR LO QUE RECIBO
void imprimir() {
  Serial.print("1: "); Serial.println(CONF_I);         // INICO
  Serial.print("2: "); Serial.println(Velocidad_MI);   // Motor I
  Serial.print("3: "); Serial.println(Velocidad_MD);   // Motor D
  Serial.print("4: "); Serial.println(INFO_ASPAS);     // Aspas
  Serial.print("5: "); Serial.println(CONF_F);         // FIN
}
